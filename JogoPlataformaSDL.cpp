//Bibliotecas necess�rias
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>

//As superf�cies
SDL_Surface *tela = NULL;
SDL_Surface *nut = NULL;
SDL_Surface *fundo = NULL;

//Atributos da Tela
const int tela_LARGURA = 640;
const int tela_ALTURA = 480;
const int tela_BPP = 32;

//A taxa de Frames
const int FRAMES_POR_SEGUNDO = 20;

//A estrutura evento
SDL_Event evento;

//Estruturas retangulares da Fase
SDL_Rect parede[30];


class EntidadeBasica
{
    protected:
        //Caixa de Colis�o da Entidade
        SDL_Rect caixa;
        //Velocidade da entidade
        int xVel, yVel;
        SDL_Surface *image = NULL;

    public:
        EntidadeBasica();
        EntidadeBasica(int X,int Y,const int LARGURA,const int ALTURA);

        //Checar todas as colis�es com outras entidades
        bool checar_todas_colisoes();

        //Mover a entidade
        void move();

        //Mostrar o personagem na tela
        void mostrar();

        //Carregar a imagem do personagem
        void definir_imagem(std::string caminho);

        //Pegar a caixa de colis�o do personagem
        SDL_Rect getcaixa();

        //Pegar velocidade em Y
        int getYVel();

        //Pegar velocidade em X
        int getXVel();

        //Pegar posi��o y do personagem
        int getY();

        //Pegar posi��o x
        int getX();

        //Setar a velocidade em y do personagem
        void setYVel(int novoYVel);

        //Setar a velocidade em X do personagem
        void setXVel(int novoXVel);

        //Setar a posi��o X do Personagem
        void setX(int novoX);

        //Setar a posi��o y do personagem
        void setY(int novoY);

        //Setar a posi��o X Y do personagem
        void setXY(int novoX, int novoY);



};
//Fun��o b�sica para carregar uma imagem e otimiza-la para ser usada
SDL_Surface *carregar_imagem( std::string filename )
{
    //A superf�cie em que a imagem � carregada
    SDL_Surface* imagemCarregada = NULL;

    //A superf�cie em que a imagem otimizada ficar�
    SDL_Surface* ImagemOtimizada = NULL;

    //carregar a imagem
    imagemCarregada = IMG_Load( filename.c_str() );

    //Caso a imagem conseguiu ser carregada
    if( imagemCarregada != NULL )
    {
        //Otimizar a imagem carregada
        ImagemOtimizada = SDL_DisplayFormat( imagemCarregada );

        //Libertar a superf�cie da imagem n�o otimizada
        SDL_FreeSurface( imagemCarregada );

        //Caso a superf�cie esteja otimizada esteja carregada
        if( ImagemOtimizada != NULL )
        {
            //Color key da superf�cie
            SDL_SetColorKey( ImagemOtimizada, SDL_SRCCOLORKEY, SDL_MapRGB( ImagemOtimizada->format, 0, 0xFF, 0xFF ) );
        }
    }

    //Retorna a Imagem j� otimizada
    return ImagemOtimizada;
}
//Fun��o b�sica para aplicar uma imagem � superficie
void aplicar_superficie( int x, int y, SDL_Surface* fonte, SDL_Surface* destino, SDL_Rect* clip = NULL )
{
    //Segurar offsets
    SDL_Rect offset;

    //Definir
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( fonte, clip, destino, &offset );
}

//Checando a colis�o das caixas dos personagens
bool checar_colisao( SDL_Rect A, SDL_Rect B )
{
    //Os lados do Retangulo
    int esquerdaA, esquerdaB;
    int direitaA, direitaB;
    int cimaA, cimaB;
    int baixoA, baixoB;

    //Definindos os lados do rect A
    esquerdaA = A.x;
    direitaA = A.x + A.w;
    cimaA = A.y;
    baixoA = A.y + A.h;

    //Definindo os lados do rect B
    esquerdaB = B.x;
    direitaB = B.x + B.w;
    cimaB = B.y;
    baixoB = B.y + B.h;

    //Caso algum lado de A toque o outro de B
    if( baixoA <= cimaB )
    {
        return false;
    }

    if( cimaA >= baixoB )
    {
        return false;
    }

    if( direitaA <= esquerdaB )
    {
        return false;
    }

    if( esquerdaA >= direitaB )
    {
        return false;
    }

    //Se nenhum dos lados de A tiver contato com os de B
    return true;
}

bool iniciar()
{
    //Inicializa a Biblioteca
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Configura a tela
    tela = SDL_SetVideoMode( tela_LARGURA, tela_ALTURA, tela_BPP, SDL_SWSURFACE );

    //Caso tenha tido algum erro em definir tela
    if( tela == NULL )
    {
        return false;
    }

    //Definir o nome da janela
    SDL_WM_SetCaption( "Em Busca Da Noz Perdida", NULL );

    //Caso tudo tenha sido inicializado corretamente
    return true;
}

void limpar()
{
    //Libertar Superficie
    SDL_FreeSurface( nut );

    //sair SDL
    SDL_Quit();
}

//Declara��o de Entidade Basica
EntidadeBasica::EntidadeBasica(){}
EntidadeBasica::EntidadeBasica(int X,int Y,const int LARGURA,const int ALTURA)
{
    //Definindo os atributos da caixa de colis�o
    caixa.x = X;
    caixa.y = Y;
    caixa.w = LARGURA;
    caixa.h = ALTURA;

    //Velocidade inicial do personagem
    xVel = 0;
    yVel = 0;
}

//Checar todas as colis�es com outras entidades
bool EntidadeBasica::checar_todas_colisoes()
{
    for(int x = 0; x < 28; x++)//Checar todas as estruturas
        if(checar_colisao(caixa, parede[x]))
            return true;
    //Checar colis�o para extremidade da tela
    if(( caixa.y < 0 ) || ( caixa.y + caixa.h > tela_ALTURA ) || ( caixa.x < 0 ) || ( caixa.x + caixa.w > tela_LARGURA ))
       {
           return true;
       }
       else
        return false;
}


//Mover a entidade
void EntidadeBasica::move()
{
    if(yVel < 10)
        yVel = yVel +1;
    //Mover a caixa na horizontal
    caixa.x += xVel;

    //Caso a entidade toque em algum outro objeto com caixa de colis�o
    if( checar_todas_colisoes() )
    {
        //mover para tr�s
        caixa.x -= xVel;
    }

    //Mover a entidade na vertical
    caixa.y += yVel;

    //Caso a entidade toque em algum outro objeto com caixa de colis�o
    if( checar_todas_colisoes()  )
    {
        //mover para tr�s
        caixa.y -= yVel;
        //Zerar Velocidade em Y
        yVel = 0;
    }

}

//mostrar as entidades na tela
void EntidadeBasica::mostrar()
{
    //mostrar a entidade
    aplicar_superficie( caixa.x, caixa.y, image, tela );
}

//Definindo qual imagem estar� aliada � entidade
void EntidadeBasica::definir_imagem(std::string caminho)
{
    image = carregar_imagem(caminho);
}

//Pega a caixa de colis�o da entidade
SDL_Rect EntidadeBasica::getcaixa()
{
    return caixa;
}
//Pega a velocidade vertical da entidade
int EntidadeBasica::getYVel()
{
    return yVel;
}
//Pega a Velocidade horizontal da entidade
int EntidadeBasica::getXVel()
{
    return xVel;
}
//Pega a posi��o vertical da entidade
int EntidadeBasica::getY()
{
    return caixa.y;
}
//Pega a posi��o horizontal da entidade
int EntidadeBasica::getX()
{
    return caixa.x;
}
//Definindo Velocidade vertical
void EntidadeBasica::setYVel(int novoYVel)
{
    yVel = novoYVel;
}
//Definindo Velocidade horizontal
void EntidadeBasica::setXVel(int novoXVel)
{
    xVel = novoXVel;
}
//Definindo posi��o horizontal
void EntidadeBasica::setX(int novoX)
{
    caixa.x = novoX;
}
//Definindo posi��o Vertical
void EntidadeBasica::setY(int novoY)
{
    caixa.y = novoY;
}
//Definindo posi��o
void EntidadeBasica::setXY(int novoX,int novoY)
{
    caixa.x = novoX;
    caixa.y = novoY;
}
//Fim da declara��o de EntidadeBasica

//O Relogio � o contador de tempo utilizado para marcar
class Relogio
{
    private:
    //O momento quanto o relogio come�ou
    int startTicks;

    //O momento quando o relogio foi pausado
    int pausedTicks;

    //O estado do rel�gio
    bool paused;
    bool started;

    public:
    //Constructor do relogio para inicializar suas vari�veis
    Relogio();

    //as A��es do rel�gio
    void start();
    void stop();
    void pause();
    void unpause();

    //Pegar o tempo do Relogio
    int get_ticks();

    //Checar o status do Relogio
    bool is_started();
    bool is_paused();
};

Relogio::Relogio()
{
    //Inicializar as variaveis do relogio
    startTicks = 0;
    pausedTicks = 0;
    paused = false;
    started = false;
}

void Relogio::start()
{
    //Come�ar o Relogio
    started = true;

    //Despausar o relogio
    paused = false;

    //Pegar o tempo do rel�gio do computador
    startTicks = SDL_GetTicks();
}

void Relogio::stop()
{
    //pausar o Relogio
    started = false;

    //Despausar o Relogio
    paused = false;
}

void Relogio::pause()
{
    //Se o Relogio estiver rodando caso j� nao estiver pausado
    if( ( started == true ) && ( paused == false ) )
    {
        //Pause o Relogio
        paused = true;

        //Calcular o momento pausado
        pausedTicks = SDL_GetTicks() - startTicks;
    }
}

void Relogio::unpause()
{
    //Caso o relogio estiver pausado
    if( paused == true )
    {
        //Despausar o relogio
        paused = false;

        //Reiniciar a contagem do momento
        startTicks = SDL_GetTicks() - pausedTicks;

        //Reiniciar a contagem do tempo pausado
        pausedTicks = 0;
    }
}

int Relogio::get_ticks()
{
    //Caso o relogio esteja rodando
    if( started == true )
    {
        //Caso o relogio esteja pausado
        if( paused == true )
        {
            //retornar a contagem do tempo pausado
            return pausedTicks;
        }
        else
        {
            //Retorna o tempo atual menos o tempo de inicio
            return SDL_GetTicks() - startTicks;
        }
    }

    //Caso o relogio nao esteja rodando
    return 0;
}

bool Relogio::is_started()
{
    return started;
}

bool Relogio::is_paused()
{
    return paused;
}

//Classe do personagem Principal o Qual voc� pode Controlar pelo teclado
class PersonagemPrincipal : public EntidadeBasica
{
    public:

    PersonagemPrincipal(int X, int Y, int LARGURA, int ALTURA)
    {
        //Configura��o inicial do personagem
        caixa.x = X;
        caixa.y = Y;
        caixa.w = LARGURA;
        caixa.h = ALTURA;

        //Velocidade inicial
        xVel = 0;
        yVel = 0;
    }
    //Pegar as teclas pressionadas e ajustar a velocidade do personagem
    void handle_input()
    {
        //Caso uma tecla seja pressionada
        if( evento.type == SDL_KEYDOWN )
        {
            //Ajustar a velocidade do personagem
            switch( evento.key.keysym.sym )
            {
                case SDLK_UP: if(yVel == 0){yVel = -11;} break;
                case SDLK_LEFT: xVel -= caixa.w / 2; break;
                case SDLK_RIGHT: xVel += caixa.w / 2; break;
                default : ;//O CodeBlocks reclama pois h� mais estados de teclas a serem analisadas
                           //as quais n�o est�o no switch
            }
        }
        //Caso uma tecla nao esteja mais pressionada
        else if( evento.type == SDL_KEYUP )
        {
            //Ajustar a velocidade do personagem
            switch( evento.key.keysym.sym )
            {
                case SDLK_LEFT: xVel += caixa.w / 2; break;
                case SDLK_RIGHT: xVel -= caixa.w / 2; break;
                default : ;
            }
        }
    }
};
//Classe para o inimigo simples
class InimigoSimples1 : public EntidadeBasica
{
    //Atributos protegidos para poderem ser aproveitados
    //por suas filhas
    protected:
    bool IA_iniciada; // Bandeira que guarda o estado do comportamento da entidade

    //Inteiros auxiliares para o comportamento da entidade
    int X;
    int Y;

    //Constructor de Inimigo simples
    public:
    InimigoSimples1();

    InimigoSimples1(int X, int Y, int LARGURA, int ALTURA)
    {
        //Configura��o inicial da Entidade
        caixa.x = X;
        caixa.y = Y;
        caixa.w = LARGURA;
        caixa.h = ALTURA;

        //Velocidade inicial
        xVel = -1;// Ajuda no comportamento da Entidade
        yVel = 0;

        IA_iniciada = false;
    }

    //Definir o estado da vari�vel para reutiliza��o da IA
    void setIA(bool novoEstado)
    {
        IA_iniciada = novoEstado;
    }

    //Comportamento da Entidade
    void AI()
    {
        if(IA_iniciada == false)
        {
            X = caixa.x;
            IA_iniciada = true;
        }
        if(caixa.x > X +30 )
            xVel = xVel*(-1);
        else if(caixa.x < X -30  )
            xVel = xVel*(-1);
    }
};
//Para melhor constru��o de unidade gr�fica b�sica o ret�ngulo
SDL_Rect setRect(int x, int y, int w, int h)
{
    SDL_Rect rect;
    //W (width) � a largura
    //enquanto h (heigth) � a altura

    rect.x = x;
    rect.y = y;
    rect.h = h;
    rect.w = w;

    return rect;
}
//Aplicar Ret�ngulo em cor cinza � tela
void aplicarRetanguloNaTela(SDL_Rect rect)
{
    SDL_FillRect( tela, &rect, SDL_MapRGB( tela->format, 0x77, 0x77, 0x77 ) );
}

int main( int argc, char* args[] )
{
    //Bandeira para Sair
    bool sair = false;

    //numero da fase
    int numeroFase = 0;

    //Instanciando personagens
    PersonagemPrincipal esquilo(530,360,17,25);
    EntidadeBasica nut(475,60,40,40);
    InimigoSimples1 cobra(460,60,45,37);
    InimigoSimples1 cobra2(320,420,45,37);cobra2.setXVel(3);
    InimigoSimples1 cobra3(140,350,45,37);cobra3.setXVel(2);

    //Declarando FPS de Relogio
    Relogio fps;

    //Iniciar biblioteca SDL
    if( iniciar() == false )
    {
        return 1;
    }

    //Carregar as imagens dos personagens
    esquilo.definir_imagem("esquilo.bmp");
    nut.definir_imagem("nut.bmp");
    cobra.definir_imagem("cobra.bmp");
    cobra2.definir_imagem("cobra.bmp");
    cobra3.definir_imagem("cobra.bmp");

    //Imagem do fundo
    fundo = carregar_imagem("floresta0.bmp");

    //Equanto a bandeira sair for falsa
    while( sair == false )
    {
        //Come�ar o fps do jogo
        fps.start();

        //Aonde os eventos do jogo ocorrem
        while( SDL_PollEvent( &evento ) )
        {
            //Manipular os enventos do Esquilo (Teclado)
            esquilo.handle_input();

            //Caso a o X da janela seja prescionado
            if( evento.type == SDL_QUIT )
            {
                //sair o program
                sair = true;
            }
        }

        //Movendo personagem principal
        esquilo.move();

        //Preencher a Tela com o fundo
        aplicar_superficie(0,0,fundo,tela,NULL);

        switch (numeroFase)
        {
            case 0:
                    //x y w h
                    parede[0] = setRect( 475,395,115,45);
                    parede[1] = setRect( 417,440,225,50);
                    parede[2] = setRect( 195,355,215,45);
                    parede[3] = setRect( 20, 320, 135, 40);
                    parede[4] = setRect( 90,280, 40, 55);
                    parede[5] = setRect( 194,226,280,40);
                    parede[6] = setRect( 215, 197, 190,65);
                    parede[7] = setRect( 290,155,115,100);
                    parede[8] = setRect( 400,115,135,60);

                    if(checar_colisao(esquilo.getcaixa(), nut.getcaixa()))
                    {
                        numeroFase++;
                        esquilo.setX(50);
                        esquilo.setY(70);
                        nut.setX(415);
                        nut.setY(400);


                    }
                    break;

            case 1:

                    parede[0] = setRect( 0,100,431,48);
                    parede[1] = setRect( 550,170,88,47);
                    parede[2] = setRect( 415,195,145,70);
                    parede[3] = setRect( 280, 210, 137, 94);
                    parede[4] = setRect( 173,246, 113, 78);
                    parede[5] = setRect( 80,200,52,29);
                    parede[6] = setRect( 10, 320, 52,74);
                    parede[7] = setRect( 66,411,220,46);
                    parede[8] = setRect( 400,435,33,33);


                    cobra.AI();cobra2.AI(); cobra3.AI();
                    cobra.move();cobra2.move(); cobra3.move();
                    cobra.mostrar();cobra2.mostrar(); cobra3.mostrar();

                    if(checar_colisao(cobra.getcaixa(), esquilo.getcaixa()) || checar_colisao(cobra2.getcaixa(), esquilo.getcaixa())
                       || checar_colisao(cobra3.getcaixa(), esquilo.getcaixa()) )
                    {
                        esquilo.setX(50);
                        esquilo.setY(70);
                    }

                    if(checar_colisao(esquilo.getcaixa(), nut.getcaixa()))
                    {
                        numeroFase++;
                        esquilo.setXY(115,440);
                        nut.setXY(25,280);

                        cobra.setIA(false);
                        cobra2.setIA(false);

                        cobra.setXY(487,280);
                        cobra2.setXY(330,220);

                    }

                break;
            case 2:
                    parede[0] = setRect( 200,430,tela_LARGURA,tela_ALTURA);
                    parede[1] = setRect( 319,376,tela_LARGURA,tela_ALTURA);
                    parede[2] = setRect( 459,327,tela_LARGURA,tela_ALTURA);
                    parede[3] = setRect( 100, 279, 306, 18);
                    parede[4] = setRect( 68,135,48,140);
                    parede[5] = setRect( 118,196,28,28);
                    parede[6] = setRect( 207,157,224,31);
                    parede[7] = setRect( 293,125,45,40);
                    parede[8] = setRect( 143,245,28,28);
                    parede[9] = setRect( 197,70,56,44);
                    parede[10] = setRect( 12,310,46,38);

                    cobra.AI();cobra2.AI();
                    cobra.move();cobra2.move();
                    cobra.mostrar();cobra2.mostrar();

                    if(checar_colisao(cobra.getcaixa(), esquilo.getcaixa()) || checar_colisao(cobra2.getcaixa(), esquilo.getcaixa()))
                    {
                        esquilo.setXY(115,440);
                    }

                    if(checar_colisao(esquilo.getcaixa(), nut.getcaixa()))
                    {
                        sair = true;
                    }
        }

        //Mostrar todas as paredes da fase na tela
        for(int x = 0; x < 28 ; x++)
            aplicarRetanguloNaTela(parede[x]);

        //Mostrar os personagens na tela
        esquilo.mostrar();
        nut.mostrar();

        //Atualizar a tela
        if( SDL_Flip( tela ) == -1 )
        {
            return 1;
        }

        //ajustar a taxa de frames do programa
        if( fps.get_ticks() < 1000 / FRAMES_POR_SEGUNDO )
        {
            SDL_Delay( ( 1000 / FRAMES_POR_SEGUNDO ) - fps.get_ticks() );
        }
    }

    //Limpar superf�cies
    limpar();

    return 0;
}
